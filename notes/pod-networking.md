# Understanding pod networking notes

---
 - In kubernetes, a pod holds an IP address.
 - There is a "pause" container in a pod that sits next to the other containers in a pod. you can see this on a worker node when you docker ps. 
 - pause container manages the IP. pause exposes the IP to the pod and to the containers on the same pod. 
 - in multi-container scenarios, all the pods are connected to the same IP and can communicate via loopback address, or IPC and volume connections.
 ---
 # Pod-to-pod communication
  - Pods are accessible on thier own network.
  - Pods can communicate directly with one another and are all in the same network  namespace
  - CNI provides a framework in which networking modules can be used to establish communication according to different needs
  - if no limitations are implemented, all pods can communicate to all other pods without limitations. this may not be desirable.
---
# Network policies
  - Network polices make it possible to implement restrictions on direct traffic between pods
  - using a network policy is only possible if the network plugin used offers the required support
  


