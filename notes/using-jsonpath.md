# Accessing object data with JSONPath

- kubectl supports JSONPath.
- this allows you to write expressions, access, filter, sort and format object data.
- you can perform precise operations on the objects

### Examples:
 - list all pod names? \
 `kubectl get pods -o jsonpath='{.items[*].metadata.name}'` \
 .items = a collection of objects returned by the kubeapi-server. so in this case, it's a list of pods. \
 [*] indicates that this expression operates on all items in the list. \
 so, for every item in that list, we are accessing the metadata.name. this will return a list of pod names.

 - get a list of all images in use by all pods in all namespaces?
 `kubectl get pods --all-namespaces -o jsonpath='{.items[*].spec.containers[*].image}'`
   - .items[*] gets all items (for all items)
   - .spec.containers[*] selects all the containers in the spec. ( access the spec, then access all containers in the spec)
   - .image displays the image name.( for all containers in the spec, display the name)
    a visual representation, checkout jsonrep.png


# filtering objects with JSONPath

examples:
1. get all internal ip addresses of nodes in a cluster.

`kubectl get nodes -o jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}"`
to explain:
items[*] means work on all items in this object \
.status.addresses is the addresses section of the object \
[?(@.type =='InternalIP')] is a filter expression, ?() says to use a filter, @ means filer on the current object. then .type=='InternalIP' is to test if the type is equal to the string "InternalIP". then if it is an InternalIP, we get the address with .address.    

to help figure out jsonpath expressions, you need to output things as json so you can see the path flow.
`kubectl get pods -o json > pods.json` could be used to see the json and craft a JSONPath expression

2. sort object data
 - get a list of pod names sorted by name:
 ` kubectl get pods -A -o jsonpath='{.items[*].metadata.name}{"\n"}' --sort-by=metadata.name`

3. get the pod name, sort by the timestamp, an output to a custom column:
`kubectl get pods -A -o jsonpath='{.items[*].metadata.name}{"\n"}' --sort-by=metadata.creationTimestamp --output=custom-columns='NAME:metadata.name, CREATETIMESTAMP:metadata.creationTimestamp'`
