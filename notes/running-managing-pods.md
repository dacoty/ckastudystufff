# Running and managing pods

## What is a pod?
 - a wrapper around your container based application
 - one or more containers can be in a pod - typically just one but there can be more
 - in addition to containers, pods can also have resources that are associated with the execution environment that is your application(storage, networking, application configuration like env variables)
 - the pod is the unit of scheduling for the cluster. it is the scheduler's job to figure out where to allocate pods on the cluster and on the nodes based on the resources that are available.
 - when it's running, it's a process that's running in your cluster consuming resources
 - it is a unit of deployment that defines the application configuration, networking and storage resources

## Why do we need pods?
- Provides a higher level of abstraction over a container for manageability

## How pods manage containers
 - single container pods - the most common deployment scenario
 - generally a single process running in a container
 - easier application scaling, minimizing dependencies
 - basically it goes(from the outside in) node -> pod -> container -> process in a container
 - multi-container pods - for tightly coupled things in a producer/consumer relationship
 - init containers - pods that have containers that set up the main application container, then shut down. they are run first.

## The relationship between controllers and pods
 - Controllers are control loops that watch the state of your cluster, then make or request changes where needed. Each controller tries to move the current cluster state closer to the desired state.
 - Controllers are Deployments, ReplicaSets, DaemonSets, CronJobs, Jobs, and StatefulSets
 - Controllers keep your apps in the desired state
 - Responsible for starting and stopping pods
 - Application scaling
 - Application recovery
 - In general, you don't want to run naked pods ( pods outside a controller ). They won't be recreated in the event of a failure over something like a deployment, which guarantees your pods will be up based on a desired state.

## Static pods
 - Pods managed by the Kubelet on nodes
 - Static pods are made with YAML manifests
 - stored in the `staticPodPath` in the kublet config
 - default is `/etc/kubernetes/manifests`
 - these are how the control plane pods are managed on the control plane nodes
 - you can edit the kubelet config here: `/var/lib/kubelet/config.yaml`
 -`staticPodPath`is watched by the kubelet on the node, so when changes to files in that path happen, the kubelet will sense and change the pods by restarting them. this also happens when you restart the node
 - kubelet controls these pods, but a mirror pod is created so you can see the pods using `kubectl get pods`. this means you can NOT control these pods by the api server. you will need to adjust the manifest files directly.

## working with pods - kubectl
 - a lot of this is covered elsewhere in notes, but as a refresher:
 - to interact with a pod/container: `kubectl exec -it POD --container CONTAINER -- /bin/bash` <-cmd prompt into a container (you specify the container if the pod has multiple containers in it)
 - to get logs : `kubectl logs POD --container CONTAINER`
 - to talk to an application that's running on a local pod network ( like a clusterIP service)and you are coming from the api server: `kubectl port-forward pod POD1 LOCALPORT:CONTAINERPORT`<- we send traffic to that localport and it pops out at the other end on the application at the container port.

## multi-container pods
 - use if you have very tightly coupled applications
 - they will be scheduled together on the same node
 - usually a requirement on some shared resource
 - usually something generating data while the other process consumes
 - don't use multicontainer pods to influence scheduling. other techniques do this ( labels on nodes and selectors on pod definitions)
 - containers are defined in pod manifests as usual, you would just add another pod in the containers spec.
## Common anti-pattern for multi-container pods
 - don't put web and database server containers in the same pod. recovery options are complicated with controllers. It also limits scaling of the pods
## Shared resources inside a pod - networking
 - Shared loopback interface, used for communication over localhost
 - they share network namespaces, so be mindful of application port conflicts. separate applications in the pod need unique ports. web on 8080, redis on 6379 for example.
## Shared resources in a pod - storage
 - Each container image has it's own file system
 - volumes/PVC's are defined at the pod level
 - these are shared amongst all the containers in the pod
 - they can be mounted into the container's file system
 - this is a common way for containers to exchange data
## init containers
 - Runs before the main application container
 - Contains utilites or setup for apps
 - Needs to run to completion before starting the main application container
 - Can have more than one per pod
 - Each is run sequentially
 - ALL init containers must run to successful completion for the pod to start
 - When an init container fails, a `restartPolicy` on the container applies. this allows you to control the restart behavior
## When to use init containers
 - run tools and utilites to set up the execution environment of your application
 - separation of duties - you can run init containers at higher priv levels that the application for sensitive tasks
 - block container startup - for example, you can ensure environment settings are in place before the application starts
## how to make pod with init containers
- in the example below, the `initContainers` are made in the pod spec before `containers` and are run in the order they are listed. in the example below, init-service will run, then init-database will run, then the main application will run. if the init containers error out, the pod won't come up.
```
apiVersion: v1
kind: Pod
...
spec:
     initContainers:
       - name: init-service
         image: ubuntu
         command: ['sh', '-c', "echo waiting for service; sleep 2"]
       - name: init-database
         image: ubuntu
         command: ['sh', '-c', "echo waiting for database; sleep 2"]
      containers:
        - name: nginx
          image: nginx
  ```
## pod lifecycle
 - Creation
  - created administratively( on its own, by the user)
  - created by a controller(e.g. deployment)
 - Transition to Running
  - Scheduled to a node
 -  from Running to Termination
  - process finished
  - process crashed
  - pod is deleted
  - pod is evicted due to lack of resources - if the node thinks the pod doesn't have enough resources, it will evict the pod, then the pod will terminated and be scheduled to run on another node.
  - node failures
  - maintenance( like kubernetes upgrade where the node is drained/cordoned)
  - no pod is "redeployed" - they are deleted and recreated
## Stopping/terminating Pods
 - a terminate command is sent to the pod via the API.
 - A grace period timer starts at the API level, to let the pod terminate gracefully
 - pod status changes to "terminating"
 - kubelet on the node sends a SIGTERM to the processes on the containers in the pod
 - pod is removed from any service endpoints and controllers are updated
 - pod is deleted
 - if the pod goes over the grace period, a SIGKILL is sent
 - API and etcd are updated
 - you can set the grace period like so: `kubectl delete pod POD --grace-period=<seconds>`
 - force deletion:`kubectl delete pod POD --grace-period=0 --force` - this will immediately delete from API and etcd
 - use this in case the app won't stop. you will have to go back and kill those processes later but you can reuse the pod name to get the application back online.
 - declaratively, you can now add `terminationGracePeriodSeconds` in the spec of the pod manifest.
 - you can tune this to give your app time to gracefully terminate.
## Persistency of pods
 - a pod is never redeployed. only deleted and recreated
 - if a pod stops, a new one is created based on it's controller. it's a brand new pod with no state transitioned or transferred between the old pod and new one
 - The original pod definition is used to create new pods. we need to decouple state, but how?
 - configuration is managed externally to the pod
 - use things like secrets and configMaps which are stored in the cluster and reference them in the pod manifest.
 - you can pass environment variables into the containers
 - Data Persistency is managed externally - PV/PVC's
## container restart policy
 - a container in a pod can restart independent of the pod itself
 - applies to containers in the pod and defined in the pod spec
 - the pod is the environment the container runs in - data Persistency and app config will still be available to the container if they are decoupled from it and stored in the cluster ( see Persistency of pods above)
 - containers are not rescheduled to another node but restarted by the kubelet on that node.
 - if the application is failing over and over again, kubernetes protects itself with an exponential backoff, 10s, 20s, 40s, capped at 5 min and reset to 0 after 10 minutes of successful runtime. it increases time to restart every time and caps at 5 minutes.
 - policy settings:
  - Always - will restart all containers inside a pod ( this is the default)
  - OnFailure - non-graceful terminations ( non-0 exit codes)
  - Never - Container restarts will never occur inside a pod
## Specifying a container restart policy:
 - this is done in the manifest under `spec:`
 - be careful, this is not under `spec-containers` ( since this applies to the entire pod)
 - `restartPolicy: OnFailure` or `restartPolicy: Always` or `restartPolicy: Never`
## Defining pod health
- A pod is considered ready when all containers are ready
- but we would like to be able to understand a little more about our applications
- we can add additonal intelligence to our pod states and health
- Using Container Probes - `livenessProbe` `readinessProbe` `startupProbe`
## livenessProbe
- Continuously runs a diagnostic check on the container
- Per container setting
- on failure, the kubelet restarts the container based on the container restart restartPolicy on the pod
- this gives kubernetes a better understanding of our application
## readinessProbe
- Continuously runs a diagnostic check on the containers to determine if it's ready to receive traffic
- can also help with deployment rollouts to make sure there are a set number of pods up to support function while new versions are being rolled out
- per container setting
- Won't receive traffic from a service until it succeeds
- on failure, removes the pod from load balancing. the container is not restarted like a livenessProbe
- this can be used to protect applications that temporarily cannot respond to a request
- prevents users from seeing errors
## startupProbe
- Run a diagnostic check against the containers in a pod during pod startup
- ensuring all containers in the pod are `Ready`
- per container setting
- on startup, all other probes are disabled until the `startupProbe` succeeds
- on failure, the kubelet restarts the container based on the container restart restartPolicy on the pod
- use startup probes when you have an application that has a long startup time
- complements liveness and readiness probes
## types of diagnostic checks for probes
- exec
 - uses a process exit code ( run a command from a container and check out the exit code)
- TCP socket
 - test to see if we can open a TCP socket/port
- httpGet
 - does a get on a URL and looks at the return code between >= 200 and < 400
- all can return success, failure, or unknown
## configuring container Probes
 - initialDelaySeconds - number of seconds after the container has started before running the probes. default is 0.
 - periodSeconds - how frequently the probe runs. default interval is 10 seconds
 - timeoutSeconds - how long to wait before giving up and calling a failure. default is 1 second
 - failureThreshold - number of missed checks before failure. default is 3
 - successThreshold - after the failure of a check, this is the number of probes to be considered successful and alive. default is 1
## code example - liveness/readiness/startup probe:
 - `spec -> containers -> livenessProbe`/`readinessProbe`/`startupProbe`
 ```
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: nginx
  name: nginx
spec:
  containers:
  - image: nginx
    name: nginx
    livenessProbe:
      initialDelaySeconds: 15
      periodSeconds: 20
      tcpSocket:
        port: 8080
    readinessProbe:
      initialDelaySeconds: 5
      periodSeconds: 10
      tcpSocket:
        port: 8080
    startupProbe:
      initialDelaySeconds: 5
      periodSeconds: 10
      tcpSocket:
        port: 8080
```
 NOTE: formatting above is bad. also, if you use all probes in the container spec, the startupProbe has to pass before any of the other probes activate.
