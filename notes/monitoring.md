# Monitoring in Kubernetes

In kubernetes, you monitor your systems to observe/understand what's happening in your cluster and workloads. you also measure the impact of changes in your environment, and understanding your resource limits.

this topic will focus on the metrics-server and getting that up and running. a common production stack would be prometheus and grafana though


### Kubernetes metrics server
- provides resource metrics for pods and nodes
- can access point-in-time performace data ( meaning, "what's happening now")
- collects CPU and Memory metrics from kubelet and exposes via the api-server
- you can access via `kubectl top pods` or `kubectl top nodes`
- don't use metrics-server stuff for building 3rd party monitoring platforms. this stuff is meant to be fed to the scheduler and HPA/VPA actions


### installing metrics-server
 - Log into the control plane
 - get the metrics server: `wget https://github.com/kuberetes-sigs/metrics-server/releases/download/VERSION/components.yaml` ( get the latest version)
 - you may need to alter these lines in the container's arguments, line 90:
  - --kubelet-insecure-tls
  - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname

- apply the file `kubectl apply -f components.yaml`
- check if metrics-server is running: `kubectl get pods --namespace kube-system`
- check the nodes: `kubectl top nodes`
- if the metrics server is acting up, look at the logs for the server
`kubectl logs --namespace kube-system -l k8s-app=metrics-server`

### kubectl top

- you can use label selectors for filtering, like so:
 - `kubectl top pods -l app=myapp`
- you can also primitive sort:
 - `kubectl top pods --sort-by=cpu`
 - `kubectl top pods --sort-by=memory`
- per container utilization metrics ( if you want to see all the container resources in a pod)
 - `kubectl top pods --containers`
