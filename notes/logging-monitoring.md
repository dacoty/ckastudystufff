 # logging in kubernetes

## containers and pods

### How logging works in containers and pods.
 - Containers log to stdout and stderr. Where the streams go depend on how the runtime is configured.
 - Docker sends logs to `/var/log/Containers`
 -  when containers are running on a node, the last 2 logs are retained on the node. the current running log and the previous log if the container has been restarted.
  - when the pod the containers ran on is removed, so are the log files.
  - because this can cause gaps in data, the logs are usually shipped to a log aggregation system so they are retained. common tooling is fluentd for aggregation, elasticsearch for searching, and Kibana for visualization.
  -kubectl logs - kubelet reads the log files directly from the container. --previous accesses the previous log if it's available on the node.
  - logs should be rotated as well using logrotate or something similar

  #### Accessing logs:
  `kubectl logs <podname>` <- get the logs from a pod. you can also get the logs from nodes and control plane. \
  `kubectl logs <podname> -c <containername>` <- get logs from a specific container for multicontainer pods. \
  if kube-api server is down, you can log into the node and do `docker logs <container name>` to get the log. \
  if docker is not available: you can get to the logs by doing `tail /var/log/containers/<containernameContainerid`

  ### common scenarios:
   - get all logs matching a container with a label and pipe to a file: \
  ` kubectl logs --selector app=web --all-conainers > weblog.txt` \
  - to follow a log, you can do `kubectl logs <pod name> --all-containers --follow` \
  - to tail a log, you can do `kubectl logs <pod name> --all-containers --tail 5`

## nodes
### how logging works on nodes
 - nodes run `kubelet and kube-proxy`
 - kublet runs as a systemd service usually, meaning that the logs from kubelet are in journald
 - see them using `journald kubelet.service`
 - non systemd based systems, find the log at /var/log/kubelet.log
 - kube-proxy is usually a pod, you can find the logs at `kubectl logs kube-proxy`. you will find enpoint state changes and iptables logs here.
 - if kube-proxy isn't a pod, you can find the logs at /var/log/kube-proxy
 - as you are troubleshooting things, also check out the OS logs like /var/log/messages or the kernel log /var/log/kern.log



## control plane
### how logging works on the control plane

- if all your control plane components are running as pods, you can view them with `kubectl logs -n kube-system PODNAME`
  for refresher, the control plane components are  `kube-scheduler, kube-apiserver, kube-controller-manager, and etcd`
- if api-server isn't available, and if the components are pods, you can use docker instead : `docker logs <containername>` or in /var/log/containers on the node.
- if they are running as systemd services, the logs are under `journald`
- non systemd logs will be at `/var/log/<component>.log`



## events
### how logging works for resources defined in a cluster
things like pod evictions, node status changes, scheduling operations, scaling decisions...
- `events` log changes in resource states
- this should be the first place you look when things go sideways
- `kubectl get events` will get you clusterwide events on any resource in the cluster independent of it's lifecycle
- if you need to focus on a specific resource, use `kubectl describe <type> <name>`( the resource needs to exist)
- there is only 1 hour retention of these events, you may want to forward those events to log aggregator

### common commands:
 - filter events based on warnings : `kubectl get events --field-selector type=Warning, reason=Failed`
 - watch events as they happen: `kubectl get events --watch &`
