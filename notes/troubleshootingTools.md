# Troubleshooting tools

`kubectl logs` - helpful when containers/pods crash or something goes wrong.
`kubectl events` - useful when troubleshooting deployed workloads or the cluster state itself, like node readiness.
`systemctl` - help control/manage systemd units and services. this isn't kubernetes specific, more of a linux thing. commonly used with working with the kublet or the container runtime
`journalctl` - used to access the login information for a systemd service
 system logs - OS logs to help troubleshoot issues. like kern.log or /var/log/messages

 ---


 # Troubleshooting Nodes

  - is the node online?
  - is the node reachable over the network?
  - systemd - is it starting kubelet and docker? are they set to startup at boot?
  - is kube-proxy running on the node? this runs as a daemonset in the kube-system namespace
  - is the kubelet running and talking to api-server? if not, what's preventing that?


  ---

  # Managing the kubelet with systemd

  - get the status of the systemd unit : `systemctl status kubelet.service`
  - set it to start on boot: `systemctl enable kubelet.service`
  - start a kubelet service: `systemctl start kubelet.service`
  - systemd unit config ( in ubuntu): `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf`
  - kubelet componentConfig : `/var/lib/kubelet/config.yaml`



  # Troubleshooting the control plane
  - same steps as above to start. but some changes with control plane.
  - control plane pods are static files, are the static pod manifests present?
  - they should be in `etc/kubernetes/manifests` on the control plane node. there is a static file for each control plane component - etcd, apiserver, controller-manager, scheduler.
  -`var/lib/kublet/config.yaml` has the `staticPodPath` of those files, in case the defaults are not used.
  - are the manifest files accessible?
  - are they error free/not misconfigured?
  - are all the components running? look at `kubectl get componentstatuses`

  
