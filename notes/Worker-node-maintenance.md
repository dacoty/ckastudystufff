# Worker node maintenance

## general procedure
 - drain and cordon the node
  - `kubectl drain` <- this cordons the node, then drains it.
 - perform your maintenance ( os upgrades, hardware, etc)
 - once done, uncordon the node so it can be used again

 ### notes
  - you can reboot the node to do this, but it will be out of action for 5 minutes. better to drain.
  - don't bring down so many nodes you will degrade service. plan for CPU/mem resources to be good to live without the node until maintenance is completed.
  - you can also bring fresh nodes online and drain the old nodes before deleting.
