# using the kubernetes API

### This is gonna be a lot of stuff.


### kubernetes  API and the API server
- Single surface area over the resources in the cluster
- you use API objects to model the system and build the workloads you want to deploy in the cluster.
- API objects are a collection of primitives to represent your system's state, and enables configuration of state.
- API server is the sole way to interact with the cluster and the sole way kubernetes interacts with the cluster.


### Kubernetes API server
 - Client/Server architecture
 - RESTful API over HTTP using JSON. meaning the API passes JSON around over HTTP
 - client submits requests over HTTP/HTTPS, Server responds to the request
 - Stateless - changes are not stored in the API server. The data is serialized and put into etcd

### Control plane and API Server
 - API objects are persistent entities in Kubernertes - the API itself is stateless, but the objects it will make will be stored in etcd.
 - API objects represent the state of the system.
