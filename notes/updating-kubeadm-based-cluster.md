# Updgrading kubeadm based cluster

Order of operations:
 - upgrade master/control plane nodes first
 - upgrade worker nodes afterwards

 important to note:
  - you can only upgrade minor version ( e.g. 1.17 to 1.18)
  - upgrades have to be sequential ( you can't do 1.16 to 1.18, you have to go 1.16 to 1.17, then 1.17. 1.18)
  - you should read the release notes about features and read the changelog

---

  # Upgrading the control plane
   - Update the kubeadm package using apt-get
    - `sudo apt-mark unhold kubeadm`
    - `sudo apt-get update`
    - `sudo apt-cache policy kubeadm`
    - `sudo apt-get install kubeadm=$TARGET-VERSION`
    - `sudo apt-mark hold kubeadm`

   - Drain the master node of any non-control plane pods. ` kubectl drain c1-master-1 --ignore-daemonsets`

   - Run `sudo kubeadm upgrade plan`
   - run `sudo kubeadm upgrade apply v$TARGET_VERSION` <- this pre-pulls images and updates certs. this also pulls down new static pods in
   /etc/kubernetes/manifests and moves the old ones to /etc/kubernetes/temp. kublet will read the new manifests and restart with new pods.

   - uncordon the master node: `kubectl uncordon c1-master-1`

   - update kubelet and kubectl using apt-get
     - `sudo apt-mark unhold kubelet kubectl`
     - `sudo apt-get update`
     - `sudo apt-get install kubelet=$TARGET-VERSION kubectl=$TARGET_VERSION`
     - `sudo apt-mark hold kubelet kubectl`
---
 # Upgrading the worker nodes

 - Drain the node ` kubectl drain worker-1 --ignore-daemonsets`
 - log into the node directly(using ssh it seems)

 - Update the kubeadm package using apt-get
   - `sudo apt-mark unhold kubeadm`
   - `sudo apt-get update`
   - `sudo apt-get install kubeadm=$TARGET-VERSION`
   - `sudo apt-mark hold kubeadm`

 - Upgrade the node using kubeadm
  - `sudo kubeadm upgrade node`

- update kubelet and kubectl using apt-get
  - `sudo apt-mark unhold kubelet kubectl`
  - `sudo apt-get update`
  - `sudo apt-get install kubelet=$TARGET-VERSION kubectl=$TARGET_VERSION`
  - `sudo apt-mark hold kubelet kubectl`
- log out of the node 
- uncordon the node: `kubectl uncordon worker-1`
