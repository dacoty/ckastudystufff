# How to backup etcd

- backup with snapshot using etcdctl
- backups should be secured and encrypted to protect sensitive info
- copy offsite as soon as possible
- don't store passwords in k8s secrets. secrets are not encryped, only hashed and can be unhashed with base64 decoding
- you don't want to do this manually normally, you should schedule backups with a CronJob Kubernetes object
- data path is /var/lib/etcd on the master node in the cluster( for training /lab purposes)
- hostpath is mounted to a pod in the workers to get to the etcd cluster

# Getting etcdctl

 - you can download it from github
 - you can copy it from etcd pod in the control node
 - run etcdctl from an etcd container you run directly


 # Run the backup:
 This should be run on the etcd cluster node, or if it's in the master node, on the master. you specify the version, and the ca, cert and key locations from the local directory. then you tack on snapshot save and save to a location.
 you may want to save in the /var/lib directory somewhere.

formatting note: i added a forward slash in the command as it's multiline but atom is putting it all on one line. lame.

  - `ETCDCTL_API=3 etcdctl --endpoints=https://127.0.0.1:2379 \
   --cacert=/etc/kubernetes/pki/etcd/ca.crt \
   --cert=/etc/kubernetes/pki/etcd/server.crt \
   --key=/etc/kubernetes/pki/etcd/server.key \
   snapshot save /var/lib/dat-backup.db`

  - to verify the backup: `ETCDCTL_API=3 etcdctl --write-out=table \
   snapshot status /var/lib/dat-backup.db`

# Restoring etcd

Scenario: single server pod based etcd - running on the master node.
DO THIS ON THE MASTER

1. `ETCDCTL_API=3 etcdctl snapshot restore /var/lib/dat-backup.db`
this will restore the data to current directory under default.etcdctl

2. if it exists, `mv /var/lib/etcd /var/lib/etcd.old`
3. docker stop the container that is running etcd pod
4. `mv ./default.etcd /var/lib/etcd`
5. wait for the etcd container to be restarted

this can be tested fully by:
 - create a secret
 - `kubectl get secret` to confirm the secret is there
 - backup the etcd database
 - delete the secrets
 - restore the database/wait for etcd pod to come back up
 - `kubectl get secret` to see if the secret is restored.


# Another way to backup and restore:
 - you can backup as normal but restore with this one-liner:
 ` ETCDCTL_API=3 etcdctl snapshot restore /var/lib/dat-backup.db --data-dir=/var/lib/etcd-restore`
 the name of the data dir is up to you.
  - change the static pod manifest(/etc/kubernetes/manifests/etcd.yaml). backup the file just in case.
  - change the `data-dir, volumeMounts and volumes paths` to match the paths of the new directory
  - kubelet will sense the change and restart the pod automatically
  
