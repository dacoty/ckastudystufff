# kubernetes API objects

- persistent entites in kubernetes
- they rep the state of the system

### Objects - key players:
 Kind - `Pods Services Deployments PersistentVolumes` \
 Group - `core, apps, storage` - this is a way to group like objects together \
 Version - Schema version: `v1, beta, alpha`

 ### Working with objects:
  - Imperative configuration : in-line command line creation.
  - Declarative configuration: make a manifest file - create a file in yaml/json and do `kubectl apply -f file`


  ### Basic Pod manifest example:
```
apiVersion: v1        <- specify the version of the API
kind: Pod             <- declare what object
metadata:             <-labels/annotations go here
  name: nginx-pod     <- what is the pod name?
  labels:
    app: nginx        <- adding a label here
spec:                 <- technical spec of the pod
  containers:         <- define a container ( as a list, look at the - on the next line)
    - name: nginx     <- what are we calling the container?
      image: nginx/latest <- image from dockerhub or registry
```
 - every object needs apiVersion, kind, metadata, and spec.
 - read more here: kubernetes.io/docs/reference/kubernetes-api

 ### testing without deploying
 - you can use dry-run parameter when you create a resource.
 - Server-side: processed as a typical request, but Requests wil not be persisted in storage
 - client side just writes the object to stdout and won't create the object at all. client side is good for validating syntax and piping to a file

 ### examples:
  - `kubectl apply -f deployment.yaml --dry-run=server`
    - this will send the command to the API server to validate the objects being made in the manifest
  - `kubectl apply  -f deployment.yaml --dry-run=client`
    - this will validate the objects but not send anything to the server
  - Imperative file creation:
    -   `kubectl create deployment ngnix --image=nginx -o yaml --dry-run=client > deployment.yaml`
      - this will create a starter yaml file to build an object( deployment) which you can modify to suit your needs.


### using diff
 - diff is used to generate the differences between resources running in a cluster and resources defined in a manifest file or stdin
  - `kubectl diff -f newdeployment.yaml`


example commands:

  `kubectl config get-contexts` <- check the contexts you have
  `kubecl cluster-info` <- shows the API endpoint you are pointing to.
  `kubectl api-resources`<- list of all API resources available on the cluster, including short name, api version, and kind.
  `kubectl explain pods` <- explain a resource
  `kubectl explain pod.spec.containers` <- view the pod spec containers section
  `kubectl explain pod.spec` <- view the pod spec section

  NOTE: if you kubectl explain something and the output is an object, you can further dig down and describe. for example, kubectl explain pods has spec as an object, and kubectl describe pod.spec has objects, and you can go down the tree.



  ### API groups and versioning

  - enable better organization of resources
  - API groups
    - Core API(Legacy Group): things like `Pod Node Namespace PersistentVolume PersistentVolumeClaim`
  - Named API groups
    - part of the api object's URL in API Requests
    examples:
    - `apps` <- this is where you find `Deployment`
    - `storage.k8s.io` <- `StorageClass`
    - `rbac.authorization.k8s.io` <- `Role, ClusterRole`
ref: kubernetes.io/docs/reference/kubernetes-api


### API versioning
- API is versioned, provides stability for existing implementations
- enables backward compatibility and enable forward change
- moves from alpha -> beta -> stable
- there is no direct relation to k8s release versions

#### Alpha/Experimental:
 - V1Alpha1
 - Early Release/bleeding edge
 - disabled by default in the api server - you need to enable this in the server
 - for testing only
 - could have breaking changes

#### Beta/Pre-Release:
 - V1beta1
 - completely tested
 - considered safe but still in test
 - more stable API Objects
 - Feedback encouraged

#### Stable/GA
 - V1
 - Backwards compatible
 - production ready

 # api query examples
  - `kubectl api-resources` <-get a list of resources.
  - `kubectl api-resources --api-group=apps` <- find api-groups that are "apps" ( look at the APIVERSION category)
  - `kubectl explain deployment --api-version apps/v1 | more` <-look up details on a specific deployment and version
  - `kubectl api-versions | sort | more`<- print the supported API versions and groups in alpha


  # API anatomy
   - API is RESTFul
   - we send a request, and a path/object type
   - api commits to etcd and send a request back
   - you typically use kubectl but any http client that respects the API will work too. this allows you to create your own tooling to interact with the cluster and create Objects. `curl` is a popular other way to query the API
   - Request = VERB + URL(resource location)
   - GET: gets the data for a specific resource - think `kubectl get`
   - POST : create a resource
   - DELETE: delete a resource
   - PUT: Create or update existing resource
   - PATCH: Modify the specified fields of a resource

   ### Special API requests:
   - LOG: retrieve logs from a container or pods ` kubectl logs`
   - EXEC: exec a command in a container to get the output `kubectl exec`
   - WATCH: Change notifications on a resource with streaming output.
      - Each resource has a resourceVersion
      - Watches are started on that version
      - Notifications are sent to clients watching that version `kubectl --watch`


  # API Resource Location(API Paths)
 - Core API:
    `http://apiserver:port/api/$VERSION/$RESOURCE_TYPE`
 - if it's in a namespace:
    `http://apiserver:port/api/$VERSION/namespaces/$NAMESPACE/$RESOURCE_TYPE/$RESOURCE_NAME`
 - API group:
    `http://apiserver:port/apis/$GROUPNAME/$VERSION/$RESOURCE_TYPE`
 - if it's in a namespace:
    `http://apiserver:port/apis/$GROUPNAME/$VERSION/namespaces/$NAMESPACE/$RESOURCE_TYPE/$RESOURCE_NAME`


# Response Codes from the API Server
 - 2XX
  - 200 - OK
  - 201 - Created resource Created
  - 202 - Accepted response was Accepted
- 4XX
    - 401 - Unauthorzed - didn't auth to the server correctly
    - 403 - Access Denied - probably due to security polices on the resource and the user
    - 404 - Not Found
- 5XX
    - 500 internal server error

# request anatomy

 - Connection
  - can you make a connection?
    - HTTP over TCP
    - TLS Encryption
 - Authentication
  - Are you a valid user?
    - auth plugin ( cert, password, token, etc)
    - Modular
    - if you can't auth, you get a 401

 - Authorization
  - can you perform the requested action?
    - verb on the resource
    - default deny - you need to be allowed to call the specific resources
    - if you can't, it's a 403 error


 - Admission Control
  - Administrative control over the request via an admission controller
  - additional code that intercepts the call to the API before it Persists
  - admission controller can modify Objects
  - admission controller performs validation

  ## examples of calls
  - increase the verbosity of a calls
  `kubectl get pod hello-world -v 6` <- verbosity level 6: you can see the REST call in verbose mode
  `kubectl get pod hello-world -v 7` <-same as 6 but add http headers
  `kubectl get pod hello-world -v 8` <-same as 7 but add response headers and trunc resp body
  `kubectl get pod hello-world -v 9` <- same as 8 but add full response. focus on the bottom and look for metadata

 - set up a kubectl proxy session ( this authenticates you to the server using local kubeconfig)
 `kubectl proxy &` <- & backgrounds the process.
 `curl http://localhost:8001/v1/namespaces/default/pods/hellp-world | head -n 20` <- first 20 lines

 - watch all pods on the resourceVersion and default namespace and put the watch in the background
  -  `kubectl get pods --watch -v 6 &`
  - `fg` brings the process back to the foregound so you can terminate with ctrl-c
