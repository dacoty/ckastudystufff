# Managing objects with labels, annotations, and namespaces
# Organizing objects in kubernetes:
- namespaces
  - when you want to put a boundary around a resource or an object for security, naming or resource allocation.
- labels
  - when you want to act on an object or groups of objects or influence internal kubernetes operations.
- annotations
  - when you want to add additional info or metadata on an object or resource.
## Namespaces
 - Provide an ability to subdivide a cluster and it's resources
 - Conceptually, it's a "virtual cluster"
 - You deploy objects into a namespace
 - Provides resource isolation/organization in the cluster
 - Maybe you want to group on app environments, or you have mulit-tenant clusters, or users, application owners, teams...
 - RBAC security boundary - we can limit who can access what based on namespaces
 - Naming boundary. you can have 2 resources have the same name in different namespaces, for example.
 - A resource can only be in one namespace at a time
 - This is not related to linux namespaces.
## Working with Namespaces
 - Create/Query/Delete Namespaces
 - Operate on objects in a namespace
 - Some objects are namespaced, some are not.
 - Resources are namespaced ( pods, services, controllers)
 - Physical things are not(Nodes, PersistentVolumes)
 - `default` namespace - for when you deploy stuff and don't specify a namesapace.
 - `kube-public` - used to store shared objects between namespaces, readable by all users in the cluster. things like `configMaps`
 - `kube-system` - system pods live here, like etcd, kube-controller-manager, etc.
 - user defined namespaces - you can deploy stuff there using `kubectl create namespace` or making a yaml file.
### example usage:
  - yaml file, creating a namespace:`kubectl create namespace playgroundyaml` OR make a yaml file.
  - to specify a namespace in a object:`kubectl run mypod --namespace playgroundyaml --image=nginx` OR alter a yaml file.

  ```  
  apiVersion: v1
  kind: Namespace
  metadata:
      name: playgroundyaml

  apiVersion: v1
kind: Pod
metadata:
      namespace: playgroundyaml
  ```
other commands: \
`kubectl get namespaces` <- gets all Namespaces \
`kubectl api-resources --namespaced=true` <-tells us what resources can be in a namespace \
`kubectl api-resources --namespaced=false` <-tells us what resources can not be in a namespace \
`kubectl describe namespaces` <- namespaces in detail, specify the namespace for focused detail \
`kubectl get pods --all-namespaces` <- get all pods across all Namespaces \
`kubectl get all --all-namespaces` <-get all the resources across all Namespaces \
## labels
 - used to organize resources like pods, nodes, and more
 - label selectors are used to query objects and select them, which will return collections of Objects that satisfy the search conditions.
 - this enables us to perform operations on collections of resources like `pods`
 - labels also influence internal operations of kubernetes.
 - a label is a non-heirarchial key/value pair
 - objects can have more than one label per resource. this enables more complex representations of state and ability to query objects
 - keys must be 63 characters or less, values 253 characters or less.
## using labels
  - create resources with labels ( imperative or declarative)
  - edit existing resource labels
  - assign(add) a new label
  - overwrite an exsiting label
  examples:
  ```
apiVersion: v1
kind: Pod
metadata:
      labels:
        app: v1
        tier: prod
  name: myapp
  ```

`kubectl label pod mypod app=v1 tier=prod`
- overwriting labels : `kubectl label pod mypod app=v2 tier=debug --overwrite`
- deleting a label: `kubectl label pod mypod app-` <- minus sign removes the label
## Query using labels and selectors
 - example start : `kubectl get pods` <- in this example we are selecting all pods
 - `--show-labels` <- will show all labels
 - using a selector: `kubectl get pods --selector tier=prod` <- use the selector argument and specify the label
 - using a label with advanced query: `kubectl get pods -l 'tier in (prod, qa)'` <- list all pods with a label of tier=prod or tier=qa
 - can be reversed: `kubectl get pods -l 'tier notin (prod, qa)`<- will choose all pods that do not have the labels
 - to remind, this is not just pods, but any labeled resource in kubernetes.
## How Kubernetes uses labels internally
 - controllers and services match pods using selectors, this is how kubernetes can tell what resources match with what other resources. like which pods match with which service.
 - influence pod scheduling, e.g scheduling to specific nodes with special hardware like SSD's or GPU's
## Defining labels for deployments
 - this is done in the `spec->selector->matchLabels` section. for a deployment, pods with labels that match this will be under control of this deployment. in the `template -> metadata -> labels` section, this is where the pods get this label as part of the template stamped out by this deployment. the label here matches the matchLabels section.
## Defining labels for services
  - labels are defined in the `spec-selector` section of the service. If you are frontloading a service to a deployment, this label needs to match `template -> metadata -> labels` in the deployment. see "service-deployment-labels.png" for a visual representation of this.

## changing labels can be useful for debugging running pods.
 - Example. Suppose you have a deployment with a replicaset of 4 pods that have a certain label. if you have a pod acting up and you don't want to destroy it, you can relabel the pod and it will fall out of the management scope of the deployment. the deployment/replicaset will spin up another pod to replace it but it will still be running, and you can exec into the pod and troubleshoot it without affecting anything else. additionally, you can re-label the pod from the service selector, so it won't be under the service as well.

## Annotations
 - used to add additonal information about your cluster resources
 - mostly used by people or tooling to make decisions
 - you find things like build, release, image information exposed in easily accessible areas
 - this saves you from having to write integrations to retrieve data from external data sources
 - non heirarchial key/value pair ( like a label )
 - can't be used to query/select pods or other resources ( use labels for that)
 - data stored in annotations is used for other purposes, used by people/tooling to make decisions on the Resources
 - keys can be 63 characters
 - values can be up to 256KB, a lot of data.
## adding and editing annotations
 - declarative:
```
apiVersion: v1
kind: Pod
metadata:
     name: annotations-demo
     annotations:
      imageregistry: "https://hub.docker.com/"
      owner: David
```

- imperative: \
 `kubectl annotate pod annotations-demo owner=David` <- add an annotation \
 `kubectl annotate pod annotations-demo owner=NotDavid --overwrite` <- update an annotation
