# CKA Study Stuff
  - My study materials for CKA exam


## Repo Contents
   - Kube-env-virtualbox: A vagrant env to spin up a cluster in virtualbox
   - Kube-env-vmware: A vagrant env to spin up a cluster in vmware fusion
   - Sample Manifest files
   - Practice Questions/answers (coming soon)
   
## Pre-Requisites 
   - Vagrant
   - YAML
   - Virtualbox or VMware Fusion and the vagrant plugins for each

Kube-Envs inspired by/ripped off from KodeKloudHub(https://github.com/kodekloudhub). Check them out, the training is excellent!

