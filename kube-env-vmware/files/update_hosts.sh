#!/bin/bash
set -e

#case $HOSTNAME in
#  (kubemaster) grep "kubenode01" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts && grep "kubenode02" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts;;
#  (kubenode01) grep "kubemaster" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts && grep "kubenode02" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts ;;
#  (kubenode02) grep "kubemaster" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts && grep "kubenode01" /certified-kubernetes-administrator-course/ubuntu/vagrant/filteredhosts >> /etc/hosts;;
#esac


case $HOSTNAME in
  (kubemaster) grep "kubenode01" filteredhosts >> /etc/hosts ;;
  (kubenode01) grep "kubemaster" filteredhosts >> /etc/hosts ;;
esac